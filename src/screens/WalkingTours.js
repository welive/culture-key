import React, { Component } from 'react';
import axios from 'axios';
import {
    View,
    Image,
    StyleSheet,
    ScrollView,
    Dimensions,
    ToastAndroid
} from 'react-native';
import { Button, Text } from 'react-native-elements';

export const IMAGES = {
    events: require('../assets/images/events.jpg'),
    museums: require('../assets/images/museums.jpg'),
    theaters: require('../assets/images/theaters.jpg')
};

const windowWidth = Dimensions.get('window').width;
const IMAGES_PER_ROW = 2;

class WalkingTours extends Component {
    state = { tours: [], isLoaded: null };

    componentWillMount() {
        const navigationData = this.props.navigation;
        const color = navigationData.state.params.item;
        const url = `http://welive.nsinfo.co.rs/pesacke-ture/${color}`;
        axios
            .get(url, { timeout: 5000 })
            .then(response => {
                console.log(response.data);
                this.setState({ isLoaded: true });
                this.setState({ tours: response.data });
            })
            .catch(e => {
                console.log(e);
                this.setState({ isLoaded: false });
                ToastAndroid.showWithGravity(
                    'Problem sa internet konekcijom, pokušajte ponovo.',
                    ToastAndroid.SHORT,
                    ToastAndroid.CENTER
                );
            });
    }

    calculatedSize() {
        const size = windowWidth / IMAGES_PER_ROW;
        return { width: size, height: size };
    }

    render() {
        return (

            <Image style={styles.image} source={{ uri: 'http://novisad.travel/wp-content/uploads/2017/05/dunavska-ulica.jpg' }}>
                <Text style={[styles.box]}>
                    Posetiocima Novog Sada, koji žele da sami kreiraju način na koji će obići Novi Sad, Turistička organizacija Grada Novog Sada predlaze jednu od ponuđenih ruta u Mapi pešačkih tura.
                </Text>

                <Button
                    large
                    icon={{ name: 'directions-walk', size: 32 }}
                    buttonStyle={[{ backgroundColor: '#c62828', padding: 0 }, this.calculatedSize()]}
                    textStyle={{ textAlign: 'center' }}
                    title={'Crvena ruta'}
                />
                <Button
                    large
                    icon={{ name: 'directions-walk', size: 32 }}
                    buttonStyle={[{ backgroundColor: '#F9A825' }, this.calculatedSize()]}
                    textStyle={{ textAlign: 'center' }}
                    title={'Žuta ruta'}
                />

                <Button
                    large
                    icon={{ name: 'directions-walk', size: 32 }}
                    textStyle={{ textAlign: 'center' }}
                    title={'Plava ruta'}
                />
                <Button
                    large
                    icon={{ name: 'directions-walk', size: 32 }}
                    buttonStyle={[{ backgroundColor: '#2E7D32', padding: 0 }, this.calculatedSize()]}
                    textStyle={{ textAlign: 'center' }}
                    title={'Zelena ruta'}
                />

            </Image>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    buttonContainer: {
        alignSelf: 'stretch',
        //flexWrap: 'nowrap',
        flexDirection: 'row',
        padding: 0
    },
    box: {
        padding: 5,
        backgroundColor: 'rgba(0,0,0,.6)',
        color: 'white'

    },
    image: {
        flex: 1,
        height: null,
        width: null,
        resizeMode: 'stretch',
        padding: 0
    },
});

export default WalkingTours;
