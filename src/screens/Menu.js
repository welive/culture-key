import React, { Component } from 'react';
import { View } from 'react-native';
import { Grid, Col, Row, Icon } from 'react-native-elements';

import MenuItem from '../components/MenuItem';
import Logger from '../common/Logger';

export const IMAGES = {
  events: require('../assets/images/events.jpg'),
  museums: require('../assets/images/museums.jpg'),
  theaters: require('../assets/images/theaters.jpg')
};

const LOG = new Logger();

class Menu extends Component {
  state = {
    modalVisible: false
  };

  componentWillMount() {
    if (!__DEV__) {
      LOG.logAppStarted();
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Grid>
          <Row>
            <Col>
              <MenuItem
                onPress={() => {
                  this.props.navigation.navigate('Events');
                }}
                pictureImg={IMAGES.events}
              >
                Događaji
              </MenuItem>
            </Col>
          </Row>
          <Row>
            <Col>
              <MenuItem
                onPress={() => {
                  this.props.navigation.navigate('Museums');
                }}
                pictureImg={IMAGES.museums}
              >
                Muzeji
              </MenuItem>
            </Col>
            <Col>
              <MenuItem
                onPress={() => {
                  this.props.navigation.navigate('Theaters');
                }}
                pictureImg={IMAGES.theaters}
              >
                Pozorišta
              </MenuItem>
            </Col>
          </Row>
        </Grid>

        <Icon
          reverse
          style={styles.QuestionnaireButton}
          name="feedback"
          color="#b71c1c"
          onPress={() => {
            this.props.navigation.navigate('Questionnaire');
          }}
        />
      </View>
    );
  }
}

const styles = {
  QuestionnaireButton: {
    backgroundColor: '#b71c1c',
    borderColor: '#c62828',
    borderWidth: 1,
    height: 56,
    width: 56,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 15,
    right: 20,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  }
};

export default Menu;
