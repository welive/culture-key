import React, { Component } from 'react';
import { Linking } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';

class MuseumDetail extends Component {
  onPressButton() {
    Linking.openURL(
      this.props.navigation.state.params.museum.urlForMoreInfo
    ).catch(err => console.error('An error occurred', err));
  }
  render() {
    const { state } = this.props.navigation;
    console.log(state.params.museum);
    return (
      <Card
        titleStyle={{ fontSize: 18 }}
        title={state.params.museum.title}
        image={{ uri: state.params.museum.img }}
      >
        <Text style={{ marginBottom: 10, fontSize: 16 }}>
          {state.params.museum.description}
        </Text>
        <Button
          icon={{ name: 'info' }}
          backgroundColor="#03A9F4"
          fontFamily="Lato"
          onPress={this.onPressButton.bind(this)}
          buttonStyle={{
            borderRadius: 0,
            marginLeft: 0,
            marginRight: 0,
            marginBottom: 0
          }}
          title="Opširnije"
        />
      </Card>
    );
  }
}

export default MuseumDetail;
