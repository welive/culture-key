import React, { Component } from 'react';
import axios from 'axios';
import {
  Linking,
  ToastAndroid,
  ScrollView,
  View,
  StyleSheet,
  Image
} from 'react-native';
import { Text } from 'react-native-elements';
import HTMLView from 'react-native-htmlview';
import ProgressBar from '../components/ProgressBar';

class EventDetail extends Component {
  state = { event: {}, isLoaded: null };
  componentWillMount() {
    const navigationData = this.props.navigation;
    const tag = navigationData.state.params.item.urlForMoreInfo.split('/')[4];
    const url = `http://welive.nsinfo.co.rs/opsirnije/${tag}`;
    axios
      .get(url, { timeout: 5000 })
      .then(response => {
        console.log(response.data);
        this.setState({ isLoaded: true });
        this.setState({ event: response.data });
      })
      .catch(e => {
        console.log(e);
        this.setState({ isLoaded: false });
        ToastAndroid.showWithGravity(
          'Problem sa internet konekcijom, pokušajte ponovo.',
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
      });
  }

  onPressButton() {
    console.log(this.props.navigation.state.params);
    Linking.openURL(
      this.props.navigation.state.params.item.urlForMoreInfo
    ).catch(err => console.error('An error occurred', err));
  }

  renderCard() {
    if (!this.state.isLoaded) {
      return (
        <View>
          <ProgressBar />
        </View>
      );
    }
    return (
      <View
        style={{
          backgroundColor: 'white',
          paddingLeft: 5,
          paddingRight: 5,
          paddingBottom: 10
        }}
      >
        <HTMLView
          stylesheet={styles}
          value={this.state.event.description}
          onLinkPress={url => Linking.openURL(url)}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Image style={styles.imageStyle} source={{ uri: this.state.event.img }}>
          <View style={styles.imageTitleContainer}>
            <Text style={styles.titleStyle}>
              {this.state.event.title}
            </Text>
            <Text style={styles.subTitleStyle}>
              {this.state.event.duration}
            </Text>
          </View>
        </Image>

        <ScrollView style={{ backgroundColor: 'white' }}>
          {this.renderCard()}
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: '#2196f3' // make links coloured pink
  },
  p: {
    fontSize: 16,
    padding: 0,
    margin: 0,
    textAlign: 'justify'
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: '400',
    color: 'white',
    padding: 5
  },
  subTitleStyle: {
    fontSize: 16,
    fontWeight: '200',
    color: 'white',
    padding: 5
  },
  imageTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  imageStyle: {
    alignSelf: 'stretch',
    width: undefined,
    height: 200,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  }
});
export default EventDetail;
