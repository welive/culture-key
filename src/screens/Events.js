import React, { Component } from 'react';
import { FlatList, View, ToastAndroid, Text } from 'react-native';
import axios from 'axios';
import { List, ListItem, Card, Button } from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';
import Logger from '../common/Logger';

const month = new Date().getMonth() + 1;
const LOG = new Logger();
class Events extends Component {
  state = { events: [], isLoaded: null };

  componentWillMount() {
    const monthStr = (month < 10 ? `0${month}` : month);
    axios
      .get(`http://welive.nsinfo.co.rs/tons/${monthStr}`, { timeout: 5000 })
      .then(response => {
        if (!__DEV__) {
          LOG.logAppPersonalized();
        }
        this.setState({ isLoaded: true });
        this.setState({ events: response.data });
      })
      .catch(() => {
        this.setState({ isLoaded: false });
        ToastAndroid.showWithGravity(
          'Problem sa internet konekcijom, pokušajte ponovo.',
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
      });
  }

  renderList() {
    if (!this.state.isLoaded) {
      return (
        <View style={styles.progressBar}>
          <ProgressBar />
        </View>
      );
    }

    return (
      <View>
        <List containerStyle={{ marginTop: 0 }}>
          <FlatList
            data={this.state.events}
            keyExtractor={(item, index) => index}
            renderItem={({ item }) =>
              <Card
                containerStyle={{
                  padding: 0,
                  marginBottom: 10,
                  width: undefined
                }}
                title={item.title}
                image={{ uri: item.img }}
              >
                <Text style={{ marginBottom: 10 }}>
                  {item.duration.trim()}
                </Text>
                <Text style={{ marginBottom: 10 }}>
                  {item.description.trim()}
                </Text>
                <Button
                  icon={{ name: 'info' }}
                  backgroundColor="#03A9F4"
                  fontFamily="Lato"
                  buttonStyle={{
                    borderRadius: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0
                  }}
                  title="Opširnije"
                  onPress={() => {
                    this.props.navigation.navigate('EventDetail', { item });
                  }}
                />
              </Card>}
          />
        </List>
      </View>
    );
  }

  render() {
    return (
      <View>
        {this.renderList()}
      </View>
    );
  }
}

const styles = {
  viewStyle: {
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    paddingTop: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 20 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 18
  }
};

export default Events;
