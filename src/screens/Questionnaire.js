import React, { Component } from 'react';
import { WebView } from 'react-native';
import { NavigationActions } from 'react-navigation';

class Questionnaire extends Component {
  onNavigationStateChange(webViewState) {
    if (
      webViewState.url.includes('questionnaire-status=OK') ||
      webViewState.url.includes('questionnaire-status=CANCEL')
    ) {
      this.props.navigation.dispatch(NavigationActions.back());
    }
  }

  render() {
    return (
      <WebView
        source={{
          uri:
            'http://in-app.cloudfoundry.welive.eu/html/index.html?app=culturekey&pilotId=Novisad&&lang=SR&callback=null'
        }}
        style={{ marginTop: 10 }}
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
      />
    );
  }
}

export default Questionnaire;
