import React, { Component } from 'react';
import { Linking } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';

class TheaterDetail extends Component {
  onPressButton() {
    console.log(this.props.navigation.state.params.theater.urlForMoreInfo);
    Linking.openURL(
      this.props.navigation.state.params.theater.urlForMoreInfo
    ).catch(err => console.error('An error occurred', err));
  }
  render() {
    const { state } = this.props.navigation;
    console.log(state.params.theater);
    return (
      <Card
        titleStyle={{ fontSize: 18 }}
        title={state.params.theater.title}
        image={{ uri: state.params.theater.img }}
      >
        <Text style={{ marginBottom: 10, fontSize: 16 }}>
          {state.params.theater.description}
        </Text>
        <Button
          icon={{ name: 'info' }}
          backgroundColor="#03A9F4"
          fontFamily="Lato"
          onPress={this.onPressButton.bind(this)}
          buttonStyle={{
            borderRadius: 0,
            marginLeft: 0,
            marginRight: 0,
            marginBottom: 0
          }}
          title="Opširnije"
        />
      </Card>
    );
  }
}

export default TheaterDetail;
