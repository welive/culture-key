import React, { Component } from 'react';
import { ScrollView, View, ToastAndroid } from 'react-native';
import axios from 'axios';
import { List, ListItem } from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

class Theaters extends Component {
  state = { theaters: [], isLoaded: false };

  componentWillMount() {
    axios.get('http://welive.nsinfo.co.rs/theaters', { timeout: 5000 }).then(response => {
      this.setState({ isLoaded: true });
      this.setState({ theaters: response.data });
    }).catch(() => {
      this.setState({ isLoaded: false });
      ToastAndroid.showWithGravity('Problem sa internet konekcijom, pokušajte ponovo.', 
      ToastAndroid.SHORT, ToastAndroid.CENTER);
    });
  }

  renderList() {
    if (!this.state.isLoaded) {
      return (
        <View style={styles.progressBar}>
          <ProgressBar />
        </View>
      );
    }

    return (
      <List containerStyle={{ marginTop: 0 }}>
        {this.state.theaters.map((theater, i) =>
          <ListItem
            roundAvatar
            avatar={{ uri: theater.img }}
            key={i}
            titleStyle={styles.textStyle}
            title={theater.title}
            titleNumberOfLines={theater.title.length > 20 ? 2 : 1}
            onPress={() => {
              this.props.navigation.navigate('TheaterDetail', { theater });
            }}
          />
        )}
      </List>
    );
  }

  render() {
    return (
      <ScrollView>
        {this.renderList()}
      </ScrollView>
    );
  }
}

const styles = {
  viewStyle: {
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    paddingTop: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 20 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 18
  }
};

export default Theaters;
