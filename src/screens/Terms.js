import React, { Component } from 'react';
import { WebView, View, Alert, BackAndroid, AsyncStorage } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Button } from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Settings' })]
});
const TERMS_APPROVED = 'termsApproved';

class Terms extends Component {
  state = {
    termsApproved: true
  };
  componentWillMount() {
    this.checkIfTermsApproved();
  }

  async checkIfTermsApproved() {
    const value = await AsyncStorage.getItem(TERMS_APPROVED);
    if (value != null) {
      const rezTerms = JSON.parse(value);
      this.setState({ termsApproved: rezTerms.terms });
      if (rezTerms.terms) {
        this.props.navigation.dispatch(resetAction);
      }
    } else {
      this.setState({ termsApproved: false });
    }
  }

  async startApp() {
    await AsyncStorage.setItem(TERMS_APPROVED, JSON.stringify({ "terms": true }));
    this.setState({ termsApproved: true });
    this.props.navigation.dispatch(resetAction);
  }

  closeApp() {
    Alert.alert('Obaveštenje', 'Da li želite da ugasite aplikaciju', [
      {
        text: 'Ne',
        onPress: () => console.log('Cancel Pressed')
      },
      { text: 'Da', onPress: () => BackAndroid.exitApp() }
    ]);
  }

  renderView() {
    if (this.state.termsApproved) {
      return <ProgressBar />;
    }
    return (
      <View style={{ flex: 1 }}>
        <WebView source={{ uri: 'file:///android_asset/policy.html' }} />
        <View style={{ flexDirection: 'row', alignItems: 'stretch', justifyContent: 'center' }}>
        <Button
          title="Prihvatam"
          buttonStyle={{ backgroundColor: '#2E7D32', margin: 5 }}
          onPress={this.startApp.bind(this)}
        />
        <Button
          title="Odbijam"
          buttonStyle={{ backgroundColor: '#C62828', margin: 5 }}
          onPress={this.closeApp.bind(this)}
        />
        </View>
      </View>
    );
  }

  render() {
    return this.renderView();
  }
}

export default Terms;
