import {
  StackNavigator
} from 'react-navigation';

import Events from '../screens/Events';
import EventDetail from '../screens/EventDetail';
import Menu from '../screens/Menu';
import Museums from '../screens/Museums';
import MuseumDetail from '../screens/MuseumDetail';
import TheaterDetail from '../screens/TheaterDetail';
import Theaters from '../screens/Theaters';
import Terms from '../screens/Terms';
import WalkingTours from '../screens/WalkingTours';
import Questionnaire from '../screens/Questionnaire';

export const EventStack = StackNavigator({
  Events: {
    screen: Events,
    navigationOptions: {
      title: 'Događaji'
    }
  },
  EventDetail: {
    screen: EventDetail,
    navigationOptions: {
      title: 'Događaj'
    }
  }
});

export const MenuStack = StackNavigator({
  Settings: {
    screen: Menu,
    navigationOptions: {
      title: 'Meni'
    }
  },
  Events: {
    screen: Events,
    navigationOptions: {
      title: 'Događaji'
    }
  }
  
});

export const QuestionnaireStack = StackNavigator({
  Questionnaire: {
    screen: Questionnaire,
    navigationOptions: {
      header: null
    }
  }
});

export const TermsStack = StackNavigator({
  Terms: {
    screen: Terms,
    navigationOptions: {
      header: null
    }
  },
  Settings: {
    screen: Menu,
    navigationOptions: {
      header: null
    }
  }
});

export const MuseumStack = StackNavigator({
  Museums: {
    screen: Museums,
    navigationOptions: {
      title: 'Muzeji i Galerije'
    }
  },
  MuseumDetail: {
    screen: MuseumDetail,
    navigationOptions: {
      title: 'Muzeji i Galerije'
    }
  }
});

export const TheaterStack = StackNavigator({
  Theaters: {
    screen: Theaters,
    navigationOptions: {
      title: 'Pozorišta'
    }
  },
  TheaterDetail: {
    screen: TheaterDetail,
    navigationOptions: {
      title: 'Pozorište'
    }
  }
});

export const Root = StackNavigator(
  {
    Terms: {
      screen: TermsStack
    },
    Root: {
      screen: MenuStack
    },
    Events: {
      screen: EventStack
    },
    Museums: {
      screen: MuseumStack
    },
    Theaters: {
      screen: TheaterStack
    },
    Questionnaire: {
      screen: QuestionnaireStack
    }
    
  },
  {
    mode: 'modal',
    headerMode: 'none'
  }
);
