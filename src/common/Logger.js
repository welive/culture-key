'use strict';
import axios from 'axios';

class Logger {
  constructor() {
    this.LOG_URL = 'https://dev.welive.eu/dev/api/log/culturekey';
  }

  logAppStarted() {
    axios({
      method: 'post',
      url: this.LOG_URL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer 1494d5ad-651a-496a-bd11-91386435e1c3'
      },
      data: {
        msg: 'app started',
        appId: 'culturekey',
        type: 'AppStarted',
        duration: null,
        session: null,
        timestamp: new Date().getTime(),
        custom_attr: { appname: 'culturekey' }
      }
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }

  logAppPersonalized() {
    axios({
      method: 'post',
      url: this.LOG_URL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer 1494d5ad-651a-496a-bd11-91386435e1c3'
      },
      data: {
        msg: 'app personalized',
        appId: 'culturekey',
        type: 'AppPersonalized',
        duration: null,
        session: null,
        timestamp: new Date().getTime(),
        custom_attr: { appname: 'culturekey' }
      }
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }
}

export default Logger;
