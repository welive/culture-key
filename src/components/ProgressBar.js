import React from 'react';
import {
	View,
	ActivityIndicator,
	StyleSheet
} from 'react-native';

const ProgressBar = () => (
	<View style={styles.progressBar}>
		<ActivityIndicator size="large" color="#2196F3" />
	</View>
);

const styles = StyleSheet.create({
	progressBar: {
		flex: 1,
        justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 50
	}
});

export default ProgressBar;
