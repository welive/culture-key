import React from 'react';
import { View, Image, Text, TouchableWithoutFeedback } from 'react-native';

const MenuItem = ({ onPress, children, pictureImg }) => {
  return (
    <TouchableWithoutFeedback style={styles.container} onPress={onPress}>
      <View
        style={{
          flex: 1
        }}
      >
        <Image source={pictureImg} style={styles.image}>
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,.6)',    
              flexDirection: 'column',
              alignItems: 'stretch',
              justifyContent: 'flex-end',
              paddingBottom: 20,
              paddingLeft: 20,
              flexGrow: 0.1
            }}
          >
            <Text style={styles.headline}>
              {children}
            </Text>
          </View>
        </Image>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = {
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  image: {
    flexGrow: 1,
    height: null,
    width: null
  },
  paragraph: {
    fontSize: 14,
    color: 'white',
    fontWeight: 'bold'
  },
  headline: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white'
  }
};

export default MenuItem;
