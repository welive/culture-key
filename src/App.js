import React, { Component } from 'react';
import { View } from 'react-native';
import { Root } from './config/Router';

class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Root />
      </View>
    );
  }
}

export default App;
